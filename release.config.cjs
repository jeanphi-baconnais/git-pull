const config = {
    branches: ['main'],
    tagFormat: '${version}',
    plugins: [
      'semantic-release-gitmoji',
      '@semantic-release/git',
      '@semantic-release/gitlab',
      '@semantic-release/changelog'
    ]
  }
  
  module.exports = config
