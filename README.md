[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/jeanphi-baconnais/git-pull/-/pipeline_schedules)
[![Latest Release](https://gitlab.com/jeanphi-baconnais/git-pull/-/badges/release.svg)](https://gitlab.com/jeanphi-baconnais/git-pull/-/releases) 

# Git-pull 🤖

**Git-pull** is a simple script which allow to pull all yours git repositories.

Writted in Go, this tool use one directory, and scan all git repositories. For each repo find, tool will get the current branch and pull it.

# 💫 Demo

The first step is to create a config file with the `git-pull` command. 

![git pull demo](./img/git-pull-init.png)


After this, a `git-pull`command run a 'git pull' on all your repositories present if the root directory defined in the config file.

![git pull demo](./img/git-pull-pull.png)


## 🚀 Use 

Two commands are available :

- git-pull help : to have a short explain of the tool
- git-pull init : to init or reinit the tool. It's just need to write the directory containing all your git repositories.


## ⚙️ Installation 


### Mac OS : 

```
curl -fSLO https://gitlab.com/jeanphi-baconnais/git-pull/-/raw/main/lib/darwin/git-pull

chmod +x git-pull

./git-pull

```

### Linux : 


```
curl -fSLO https://gitlab.com/jeanphi-baconnais/git-pull/-/raw/main/lib/linux/git-pull

chmod +x git-pull

./git-pull

```

NB : you can copy this install in one of your directories defined in your `$PATH`. Or add your install directory in your `$PATH`.

## 🐳 Docker image

A docker image is available and you can get it with this command : `docker pull registry.gitlab.com/jeanphi-baconnais/git-pull:<tag>` where `<tag>`is the version of this application. See [this page](https://gitlab.com/jeanphi-baconnais/git-pull/-/tags). 

# ⚙️ Contribution

## Code of Conduct

Do not hesitate to contribute 😃  I open this project for the Hacktoberfest, I can help some people to contribute.

Please read and follow our [Code of Conduct](CODE_OF_CONDUCT.md).

# Contributors ✨

Thanks to the contributors 🙏 

- [@Grotup](https://gitlab.com/Grotup)

# How contribute

You can to contribute ? it's easy follow instruction in the [CONTRIBUTING.md file](CONTRIBUTING.md).

✨ You can also use [Gitpod](https://gitpod.io) to have a workspace ready to be used.

# 📖 License

This project is under [the Apache License 2.0](LICENSE)

